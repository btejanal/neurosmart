# NeuroSmart

### Tester la maquette
Copiez le repo sur votre ordinateur (ouvrez un terminal) :
> git clone https://gitlab.inria.fr/mnemosyne/neurosmart/tree/master

Puis ouvrez le fichier index.html (dans le dossier src_dev) avec votre navigateur favori :
(commande sous Mac OS X)
> open -a "Firefox" src_dev/index.html

### Accès Rapide

#### Google Drive
Accès à la liste des documents du Google Drive : https://drive.google.com/drive/folders/0B42D-mwhUovqRjAxVWhUVTJWRzA

### Comment créer un scénario

#### Comment organiser la structure ?


On peut prendre exemple sur le scénario existant dans le dossier /src_dev/scenarios :



      scenarios
      └── example
          ├── media
          │   ├── ConditionnementPavlovian.3gp
          │   ├── ConditionnementPavlovian.flv
          │   ├── ConditionnementPavlovian.mp4
          │   ├── ConditionnementPavlovian.webm
          │   ├── screencast0.jpg
          │   ├── screencast1.jpg
          │   ├── screencast2.jpg
          │   ├── screencast3.jpg
          │   ├── screencast4.jpg
          │   ├── screencast5.jpg
          │   ├── screencast6.jpg
          │   └── screencast7.jpg
          ├── more.html
          └── scenario.json

Celle ou celui qui crée le scénario doit créer les fichiers suivants :
- more.html
- scenario.json

(... tutoriel à finir ...)

### Infos générales

Documents disponibles dans le gitlab:
- src_maquette_initiale/ : contient les sources de la maquette initiale.
  - Pour tester la maquette (dans son navigateur préféré) ouvrir l'[index.html](src_maquette_initiale/index.html) et appuyer sur la flèche en bas à gauche de la page pour lancer la première "animation". (Il faut avoir télécharger le dossier src_maquette_initiale sur son ordinateur auparavant car on ne peut pas le lancer depuis le gitlab.)
  - On peut trouver des explications sur cette maquette ici : [src_maquette_initiale/asset/using-it.html](src_maquette_initiale/asset/using-it.html)
  - - Le futur sitye web sera ici https://mnemosyne.gitlabpages.inria.fr/neurosmart/
