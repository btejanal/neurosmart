{ 
   title :  Comment notre cerveau explore l’espace visuel ?
   author : "frederic.alexandre@inria.fr, thierry.vieville@inria.fr"
   objective : "Montrer que le cerveau a une géographie avec des zones qui correspondent à différentes fonctions sensorielles ou motrices, dans le cas des mouvements des yeux."
   audience :  "large public, collège"
   prerequisite : aucun
   description : "Si on joue à cache-cache ou recherche une proie à manger ou inversement redoute un prédateur qui pourrait surgir, on ne se contente pas de regarder la scène : on la palpe du regard tandis que notre rétine reste en alerte de toute part. Explorons comment ces deux mécanismes visuo-moteurs fonctionnent."
   model : default
   sequences : {
    introduction : [ 
      { action : showText, clear:true, text : "Mais ou est Charlie ? Si on joue à cache-cache ou recherche une proie à manger ou inversement redoute un prédateur qui pourrait surgir, on ne se contente pas de regarder la scène : on la palpe du regard tandis que notre rétine reste en alerte de toute part. Explorons comment ces deux mécanismes visuo-moteurs fonctionnent.
![Où est Charlie ? Un célèbre jeu d’exploration visuelle “Source: https://www.youtube.com/watch?v=S97JmI5VmIU”](scenarios/exploration-visuelle/media/ou-est-charlie.jpg) "}
       { action : showNav, content : [ next ] }
     ]
    la-retine : [ 
      { action : showText, clear:true, text : "La rétine est vraiment un “bout” de notre cerveau …
![Coupe schématique de la rétine](scenarios/exploration-visuelle/media/coupe-schematique-de-la-retine.png)
qui reçoit de la lumière et la transforme en influx nerveux, grâce à des cellules dont le fonctionnement se modifie avec la luminosité. Mais il y a bien plus, deux couches de neurones relient les cellules entre elles pour effectuer toute sorte de calculs liés aux contrastes de l’image et aux mouvements, c’est à dire aux variations spatiales et temporelles d’intensité lumineuse. En faisant cela, la rétine “compresse” l’image c’est à dire permet à l’information captée par plus de 100 millions de “pixels” (de points de mesure de l’image) de passer sur le nerf optique qui ne contient qu’environ 1 million de fibres. 

Ce processus est rendu possible par le fait que la rétine est complètement adaptée aux images naturelles. Mais il y a encore plus sophistiqué : la rétine dispose de cellules non-standards qui font toutes sortes de calculs permettant d’analyser de manière précoce ce qui se passe dans la scène observée:
![Tout ce que calcule notre rétine](scenarios/exploration-visuelle/media/tout-ce-que-calcule-la-retine.png)
Cela permet non seulement de transmettre une image mais aussi des évènements visuels, des alertes, qui vont être analysées par le cerveau."}
       { action : showNav, content : [ prev, next ] }
     ]

    le-thalamus : [ 
      { action : showText, clear:true, text : "Regardons maintenant le début du système visuel (de la rétine à ce qui se passe dans la partie arrière et inférieure du cerveau). On y détecte des événements visuels, en faisant interagir des opérateurs de calcul de mouvement, et des détecteurs statistiques rapides (mais imprécis) d’événements visuels. Le [cortex](http://fr.wikipedia.org/wiki/Cortex) et le [thalamus](http://fr.wikipedia.org/wiki/Thalamus) échangent des informations en boucle à partir de ces entrées pour analyser ces informations. 

Cela permet de tester différentes hypothèses perceptives et de les confronter aux connaissances a priori que possède le cerveau. Par exemple, il s’agit de détecter les proies ou des prédateurs.

![Un algorithme distribué entre la thalamus et le cortex Cette figure sera simplifiée en gardant qu’un minimum de légendes.](scenarios/exploration-visuelle/media/le-thalamus-qui-calcule.png)

L'information standard qui provient de la rétine monte au cortex (c'est la voie spécifique en rouge, qui passe par le ``core´´ du thalamus). En même temps la formation non standard, transmet des événements visuels que la ``matrice´´ du thalamus va utiliser pour moduler l'information initiale, tandis qu'à droite l'information se dirige aussi vers le colliculus pour engendrer les mouvements des yeux dont on a besoin pour explorer la scène.

De ces paradigmes de calcul émergent des fonctions cognitives à l’échelle des différentes zones du cerveau : se localiser dans l’espace ou le temps, reconnaître la catégorie d’un objet (animal, nourriture, etc.), sélectionner une action et planifier un geste. Le défi est alors d’étudier ce système complexe dans sa globalité. 

Comment mieux comprendre de tels mécanismes ?"}
       { action : setColor, object : thalamus, value : #FF0000, note: "animation à prevoir"}
       { action : setCamera, x : 45, y : 90, z : 1, d : 5 }
       { action : setColor, object : cortex-visuel, value : #FF0000 }
       { action : setCamera, x : 45, y : 90, z : 1, d : 5 }
       { action : showNav, content : [ prev, next ] } 
     ]

    le-modele : [ 
      { action : showText, clear:true, text : "Pour s’assurer que ce que nous croyons comprendre marche ``pour de vrai´´ on crée un modèle informatique qui va rassembler nos connaissances relatives aux propriétés fonctionnelles de ce système. Puis on le confronte à des expériences qui peuvent être numériques, quand on teste le comportement du modèle, par exemple dans une situation de survie au sein d’un univers virtuel, ou robotiques, quand le système est implanté dans un système mécanique incarné dans la réalité. Ici nous nous contentons de traiter des séquences d’images réalistes, regardons:

Dans la première vidéo, on visualise la simulation de cette fonction visuelle:

[![Simulation de ce qui se passe à différents endroits du système visuel](scenarios/exploration-visuelle/media/video-carvajal-1.png)](http://youtu.be/7T_yfui0N-8)

La seconde vidéo montre l’endroit où se focalise l’attention sur la scène observée.

[![Simulation d’un mécanisme d’observation visuelle active d’une scène complexe](scenarios/exploration-visuelle/media/video-carvajal-2.png)](http://youtu.be/x6-O3lcRMCI)

Sur la première vidéo, on voit la sortie d’un calcul de mouvement effectué par les cellules standard de la rétine (cellules de la voie magno-cellulaire) qui est relayée par les noyaux du centre du thalamus (core) et les cellules non-standard qui réagissent automatiquement à des certains types de mouvements (dangereux, inattendus). Le thalamus va filtrer cette information pour laisser passer au cortex (et donc aux traitements ultérieurs) la plus importante, selon le contexte (ici le cortex a la complexité de celui d’une grenouille). On dispose ainsi d’un mécanisme interactif d’alerte et d’analyse des différents événements visuels, permettant de fixer l’attention sur ce qui est le plus vital. La position de la cible est représentée par un point vert et sa zone par les points jaune et bleu. En rouge, on compare avec ce qu’un sujet humain choisit comme fixation."}
       { action : setColor, object : thalamus, value : #FF0000, note: animation a prevoir}
       { action : setCamera, x : 45, y : 90, z : 1, d : 5 }
       { action : setColor, object : cortex-visuel, value : #FF0000 }
       { action : setCamera, x : 45, y : 90, z : 1, d : 5 }
       { action : showNav, content : [ prev, next ] }
     ]

    conclusion : [ 
       { action : showText, text : "Pour en savoir plus:

* Sur le cerveau : [«le cerveau à tous les niveaux»](http://lecerveau.mcgill.ca)
* Une introduction aux neurosciences computationnelles: [La grenouille gobe t'elle aussi les cailloux ?](http://www.epi.asso.fr/revue/articles/a1302b.htm)
* Source de cette animation : [Comprendre le système le plus complexe de notre planète ?](http://www.breves-de-maths.fr/comprendre-le-systeme-le-plus-complexe-de-notre-planete)
" }
       { action : showNav, content : [ home, prev ] }
     ]
}}

