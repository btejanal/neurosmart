# Télécharger les fichiers

Il est proposé de co-développer ensemble et partager à la fois les scénarios, les modèles 3D de cerveau et les mécanismes logiciels qui permettent de faire fonctionner cette ressource. On peut très simplement [télécharger les fichiers](https://gitlab.inria.fr/mnemosyne/neurosmart/-/archive/master/neurosmart-master.zip) et travailler sur son ordinateur, mais il est aussi [ossible de profiter des mécanismes de sauvegarde et de gestion de version de [GIT](https://fr.wikipedia.org/wiki/Git).

Cela permet d'[installer un nouveau scénario](./creer-scenario.md), de [programmer son scénario](./faire-scenario.md) et de [tester son scénario](./tester-scenario.md).

### Récupérer les fichiers et créer une branche personnelle

__-1-__ Télécharger les fichiers avec la commande

    git clone https://gitlab.inria.fr/mnemosyne/neurosmart/tree/master

ou via une interface graphique:

![Vue de l'interface](./src/assets/doc/img/git-clone.png)

__-2-__ Créer une nouvelle branche avec la commande

    git branch $nom-de-votre-branche

ce qui permettra de fusionner nos travaux ensuite

### Co-travailler avec l'équipe de développement

Vous pouvez aussi souhaiter participer directement à https://gitlab.inria.fr/mnemosyne/neurosmart contactez-nous comme proposé au bas du [README](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/README.md).

