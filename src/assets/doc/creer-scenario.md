# Installer un nouveau scénario

Après avoir pu [télécharger les fichiers](./telecharger.md), voici comment _installer un nouveau scénario_, avant de [programmer son scénario](./faire-scenario.md) et de [tester son scénario](./tester-scenario.md).

__-1-__ Choisir un identifiant pour son scénario : ce sera un nom composé (par exemple _exploration-visuelle_) 

  * sans espace, ni accents, ni caractère spéciaux autre que le tiret '_', 
  * descriptif du contenu du scénario,
  * qui ne soit pas encore utilisé

Il pourra être changé ensuite.

__-2-__ Aller dans le répertoire _neurosmart/src/scenarios_ et copier-coller le répertoire de nom _modele_en le renommant avec l'identifiant du niveau scénario, par exemple

      cd neurosmart/src/scenarios ; cp -r modele exploration-visuelle

__-3-__ Copier dans le sous-répertoire media tous les fichiers images, sons, vidéo, etc.. qui seront utilisés, par exemple:

    scenarios
    └── exploration-visuelle
        ├── media
        │   ├── ma-video.3gp
        │   ├── ma-video.flv
        │   ├── ma-video.mp4
        │   ├── screencast0.png
        │   ├── screencast1.png
        │   └── screencast2.png
        ├── more.html
        └── scenario.json

et ajouter les fichiers HTML complémentaires si besoin (ici le fichier _more.html_).

On va pouvoir ensuite [programmer son scénario](./faire-scenario.md) et de [tester son scénario](./tester-scenario.md).

__-4-__ Ajouter son scénario au menu principal en ajoutant son identifiant au fichier

    neurosmart/src/playlist.json

qui contient un tableau de la forme :

    [
      { 
        "url": "scenarios/example/scenario.wjson",
        "title": "Comment modéliser un comportement Pavlovien grâce aux mathématiques ?"
      },
     ../..
    ]


